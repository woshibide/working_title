import json

with open('films.json', 'r') as f:
    data = json.load(f)

for director in data:
    for film in director["films"]:
        film['gif'] = "https://www.notion.so/path_to_gif_here"

with open('films.json', 'w') as f:
    json.dump(data, f, indent=4)

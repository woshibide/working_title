let willBeWatching = [];


document.getElementById('generate-pdf').addEventListener('click', () => {
  var doc = new jsPDF();
  const pageHeight = doc.internal.pageSize.getHeight(); // get the height of the page
  let yPos = 10; // starting y position

  willBeWatching.forEach((film, index) => {
    const lineSpacing = 8; // space between lines
    const boxPadding = 5; // padding for the box

    // Calculate the height of the next box
    const descriptionLines = doc.splitTextToSize(`          ${film.description}`, 180);
    const boxHeight = lineSpacing * (4 + descriptionLines.length) + 2 * boxPadding;

    // Check if we need to add a new page before adding the next box
    if (yPos + boxHeight > pageHeight) {
      doc.addPage();
      yPos = 10; // Reset the y position for the new page
    }

    const boxTop = yPos - boxPadding; // top of the box, updated after the check for a new page

    // set font, size and style
    doc.setFont('helvetica');
    
    yPos += lineSpacing;
    doc.setFontSize(8);
    doc.text(`// 2023, working_title`, 10, yPos);
    yPos += lineSpacing;
    // Title
    doc.setFontType('bold');
    doc.setFontSize(20);
    doc.text(`${film.title}`, 10, yPos);

    yPos += lineSpacing;

    // Director
    doc.setFontType('italic');
    doc.setFontSize(10);
    doc.text(`>> ${film.director ? film.director : 'le secret'}`, 20, yPos);

    yPos += lineSpacing;

    // Genre
    doc.setFontType('normal');
    doc.text(`>> ${film.genre}`, 20, yPos);

    yPos += lineSpacing;

    // Cast
    doc.text(`>> ${film.cast}`, 20, yPos);

    yPos += lineSpacing;
    
    // Description
    // Split description into multiple lines if it's too long
    doc.setFontSize(10);
    descriptionLines.forEach((line, lineIndex) => {
      doc.text(line, 10, yPos);
      yPos += lineSpacing;
    });
    
    // Draw the box
    doc.roundedRect(5, boxTop-10, 200, yPos - boxTop + boxPadding, 3, 3);

    // Add a line to separate each film
    if (index < willBeWatching.length - 1) {
      yPos += lineSpacing * 2; // add some space before next film
    }
  });

  doc.save('working_title_ticket.pdf');
});







// Populate the film cards
function populateFilmCards(filmsData) {
  const filmCardsContainer = document.getElementById("film-cards-container");

  for (let directorData of filmsData) {
    for (let film of directorData.films) {
      const filmCard = document.createElement('div');
      filmCard.className = "film-card";
      filmCard.innerHTML = `
      <div class="film-card-header">
      <h2>${film.title}</h2>
      <button class="watch-button">add to watchlist</button>
      </div>
      <h5>imagined by </h5>
      <h3>${directorData.director}</h3>
      <div class="card-columns">
      <div class="film-info">
      <h4>Genre: </h4> <h5> ${film.genre}</h5>
      <h4>Cast:</h4> <h5> ${film.cast} </h5>
      </div>
      <div class="description"> 
      <h5>${film.title}</h5>
      <div class="film-description">${film.description}</div>
      </div>
      </div>
      <img src="https://file.notion.so/f/s/eb399695-30e6-4d1b-9922-0b15357b3d52/output.gif?id=084fae7d-970a-47b0-a322-dc2948a85a14&table=block&spaceId=7a0e8e9c-35a8-4066-9cfc-3418614ba987&expirationTimestamp=1685895355204&signature=cY0qaMI8mjktb64HZO13vk2iUKeZlHuhbq3yC41p8wQ" alt="${film.title} image">
      `;

      filmCard.addEventListener("click", () => {
        filmCard.classList.toggle("active");
        if (filmCard.classList.contains("active")) {
          filmCard.style.zIndex = "1000";
        } else {
          filmCard.style.zIndex = "0";
        }
      });

      filmCard.addEventListener("mouseleave", () => {
        if (!filmCard.classList.contains("active")) {
          filmCard.style.zIndex = "0";
        }
      });

      const watchButton = filmCard.querySelector(".watch-button");
      watchButton.addEventListener('click', (e) => {
        e.stopPropagation(); // Prevent the film card click event
        addToWillBeWatching(film, watchButton);
      });

      filmCardsContainer.appendChild(filmCard);
    }
  }
}


// Create the genre buttons
function createGenreButtons(filmsData) {
  const genres = new Set();
  filmsData.forEach(director => {
    director.films.forEach(film => {
      genres.add(film.genre);
    });
  });

  const buttonsContainer = document.getElementById("buttons-container");
  const body = document.querySelector('body');

  genres.forEach(genre => {
    const button = document.createElement("button");
    button.innerText = genre;
    button.addEventListener("click", () => {
      filterFilmsByGenre(genre);

      // Change the website's colors based on the selected genre
      switch (genre) {
        case 'blood busters':
          body.style.backgroundColor = 'black';
          body.style.color = '#D90D0D';
          break;
        case 'home movie':
          body.style.backgroundColor = 'black';
          body.style.color = '#0339A6';
          break;
        case 'directors cut':
          body.style.backgroundColor = 'black';
          body.style.color = '#04BF20';
          break;
      }
      // Remove active class from all other buttons
      document.querySelectorAll('#buttons-container button').forEach(btn => btn.classList.remove('active'));

      // Add active class to the clicked button
      button.classList.add('active');
    });
    buttonsContainer.appendChild(button);
  });

  const clearButton = document.createElement("button");
  clearButton.innerHTML = "<h5>unsorted</h5>";
  clearButton.addEventListener("click", () => {
    document.querySelectorAll(".film-card").forEach(card => {
      card.style.display = "block";
    });

    // Reset the website's colors when the filter is cleared
    body.style.backgroundColor = 'white';
    body.style.color = 'black';

    // Remove active class from all buttons when filters are cleared
    document.querySelectorAll('#buttons-container button').forEach(btn => btn.classList.remove('active'));

  });
  buttonsContainer.appendChild(clearButton);
}


// Filter films by genre
function filterFilmsByGenre(genre) {
  document.querySelectorAll(".film-card").forEach(card => {
    const genreElement = card.querySelector(".film-info h5");
    if (genreElement.textContent.includes(genre)) {
      card.style.display = "block";
    } else {
      card.style.display = "none";
    }
  });
}


function addToWillBeWatching(film, button) {
  const index = willBeWatching.findIndex(f => f.title === film.title);
  if (index !== -1) {
    willBeWatching.splice(index, 1);
    button.textContent = '+';
  } else {
    willBeWatching.push(film);
    button.textContent = '-';
  }
  // Update the counter
  document.querySelector('#generate-pdf .counter').textContent = `(${willBeWatching.length})`;
}

document.addEventListener('DOMContentLoaded', (event) => {
  document.getElementById('counter').textContent = `(${willBeWatching.length})`;
});



// Add event listeners for button hover
const watchButtons = document.querySelectorAll('.watch-button');
watchButtons.forEach(button => {
  button.addEventListener('mouseover', () => {
    button.style.opacity = '1';
  });

  button.addEventListener('mouseout', () => {
    button.style.opacity = '0.3';
  });
});


// Load film data from JSON file
fetch("films.json")
  .then(response => response.json())
  .then(filmsData => {
    populateFilmCards(filmsData);
    createGenreButtons(filmsData);
  })
  .catch(error => console.error("Error fetching film data:", error));
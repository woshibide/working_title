# Film Festival Website

![preview of the website](./preview.png)

This repository contains the code for the ≤ working_title Film Festival website. The website is designed to provide information about the festival, including the film schedule, ticket booking, venue details, and more. 

### ≤ working_title 

The ≤ working_title Film Festival is a fictional event conceived for this project. The website brings this concept to life, offering visitors a comprehensive guide to the festival's offerings. This project is done with curation by [Bart de Baets](http://bartdebaets.nl/), for KABK GD'23
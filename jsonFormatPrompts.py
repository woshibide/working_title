import json

# load the JSON file
with open('filmPrompts.json', 'r') as file:
    data = json.load(file)
    
# New list to hold the reformatted data
reformatted_data = []

# Loop through each dictionary in the list
for film_data in data:
    # Loop through each film and reformat its data
    for film in film_data['films']:
        reformatted_data.append({
            'Director Name': film_data['director'],
            'Film Title': film['title'],
            'Film Description': film['description']
        })

# Write the reformatted data back to the file
with open('path_to_your_json_file.json', 'w') as file:
    json.dump(reformatted_data, file, indent=4)
